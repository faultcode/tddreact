import {user} from './src/user'
import jsonpath from 'jsonpath'

const result = jsonpath.query(user, '$.projects')
console.log(JSON.stringify(result))