import {add} from './calc'
import user from './user'
import jsonpath from 'jsonpath'

expect.extend({
    toMatchJsonPath(received, argument){
        const result = jsonpath.query(received, argument)

        if (result.length > 0 && result !== null){
            return {
                pass: true,
                message: () => 'matched'
            }
        } else {
            return {
                pass: false,
                message: () => `expected ${JSON.stringify(received)} to match jsonpath ${argument}`
            }
        }
    }
})


expect.extend({
    toHaveBeenJabbed(received, argument){
        const result = jsonpath.query(received, argument)

        if (result.length > 0 && result !== null){
            return {
                pass: true,
                message: () => 'vaccinated'
            }
        } else {
            return {
                pass: false,
                message: () => 'vaccination information not found'
            }
        }
    }
})


describe('jsonpath', () => {
    it('matches jsonpath', () => {
        expect(user).toMatchJsonPath('$.address')
    })
})

describe('vaccine', () => {
    it('vaccination status', () => {
        expect(user).toHaveBeenJabbed('$.vaccinated')
    })
})
