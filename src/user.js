const user = {
    name: 'Baz',
    address: 'Portsmouth, UK',
    projects: [
        { name: 'TDDReact'},
        { name: 'ThoughtWorks'}
    ]
    , vaccinated: true
}

export default user